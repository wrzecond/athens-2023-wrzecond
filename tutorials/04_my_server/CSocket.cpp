#include "CSocket.h"

CSocket::CSocket() {
    m_Id = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (m_Id < 0)
        throw SocketCreateException();
}

CSocket CSocket::Create(int id) {
    return CSocket(id);
}

int CSocket::Id() const {
    return m_Id;
}

bool CSocket::ReadMessage(std::string & message) const {
    char buffer [Constants::BUFFER_SIZE + 1];
    message = "";

    bool cont;
    int bytesRead, bytesTotal = 0;
    do {
        bytesRead = (int) recv(m_Id, buffer, Constants::BUFFER_SIZE, 0);
        if (bytesRead <= 0)
            break;
        bytesTotal += bytesRead;

        cont = buffer[bytesRead - 1] != '\0';
        buffer[bytesRead] = '\0';
        message += buffer;

        if (Constants::DEBUG)
            printf("Read %s from %d\n", buffer, m_Id);
    }
    while (cont);

    return bytesTotal > 0;
}

bool CSocket::SendMessage(uint32_t messageId, const std::string &message) const {
    if (Constants::DEBUG)
        printf("Sending %u to %d\n", messageId, m_Id);
    ssize_t sentMID = send(m_Id, &messageId, sizeof(messageId), MSG_NOSIGNAL);
    if (message.empty()) // sending only the id
        return sentMID == sizeof(messageId);
    if (Constants::DEBUG)
        printf("Sending %s to %d\n", message.c_str(), m_Id);
    ssize_t sentMsg = send(m_Id, message.c_str(), message.size() + 1, MSG_NOSIGNAL); // send also '\0'
    return sentMsg == message.size() + 1;
}

bool CSocket::operator < (const CSocket & other) const {
    return m_Id < other.m_Id;
}
