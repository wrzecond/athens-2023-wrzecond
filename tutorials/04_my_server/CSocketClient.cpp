#include "CSocketClient.h"

std::map<std::string, uint32_t> CSocketClient::m_Commands = {
    {Constants::COMMAND_USERNAME, Constants::MESSAGE_USERNAME},
    {Constants::COMMAND_CHG_CHL, Constants::MESSAGE_CHG_CHL},
    {Constants::COMMAND_PUBLIC, Constants::MESSAGE_PUBLIC},
    {Constants::COMMAND_DM, Constants::MESSAGE_DM}
};

std::map<uint32_t, std::string> CSocketClient::m_Messages = {
    {Constants::RESPONSE_NO_USERNAME, "Error whilst trying to change username."},
    {Constants::RESPONSE_UNKNOWN_USER, "Error: trying to message unknown user."},
    {Constants::RESPONSE_USERNAME_CHANGED, "Username changed successfully."},
    {Constants::RESPONSE_CHANNEL_CHANGED, "Channel changed successfully."},
    {Constants::RESPONSE_TAKEN_USERNAME, "Error: this username is already taken."},
};

CSocketClient::CSocketClient() : CSocket(), m_Stopped(false) {}

bool CSocketClient::Connect(const char * hostname, int port) {
    socklen_t socketAddressLen;
    socketAddressLen = sizeof(struct sockaddr_in);

    struct sockaddr_in serverAddr = {};
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_port = htons(port);

    struct hostent *host;
    host = gethostbyname(hostname);
    memcpy(&serverAddr.sin_addr, host->h_addr,host->h_length);

    // Connection
    return connect(m_Id, (struct sockaddr *) &serverAddr, socketAddressLen) >= 0;
}

void CSocketClient::Reset() {
    m_Id = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
}

bool CSocketClient::SetUsername (const std::string & uname) {
    std::string username = uname;
    if (username.size() > Constants::BUFFER_SIZE / 4)
        username = username.substr(0, Constants::BUFFER_SIZE / 4);
    if (!SendMessage(Constants::MESSAGE_USERNAME, username))
        return false;

    uint32_t answer;
    if (!ReadMessage(answer))
        return false;

    switch (answer) {
        case Constants::RESPONSE_USERNAME_CHANGED:
            return true; // success
        case Constants::RESPONSE_TAKEN_USERNAME:
            printf("Error: username taken.\n");
            return false; // failure
        default:
            printf("Error: unknown response: %u\n", answer);
            return false; // failure
    }
}

void CSocketClient::Receiver() {
    while (true) {
        uint32_t messageId;
        if (!ReadMessage(messageId))
            return;

        std::string message;
        switch (messageId) {
            case Constants::MESSAGE_END:
                m_Stopped = true; // stopped by server
                return;
            case Constants::RESPONSE_MESSAGE:
                if (!ReadMessage(message)) // error, end
                    return;
                printf("%s\n", message.c_str());
                break;
            default:
            {
                auto it = m_Messages.find(messageId);
                if (it == m_Messages.end()) {
                    printf("Unrecognized message from server: %u.\n", messageId);
                    return;
                }

                printf("%s\n", it->second.c_str());
                break;
            }
        }
    }
}

void CSocketClient::Start() {
    m_Receiver = std::thread(&CSocketClient::Receiver, this);
    std::string message;
    while(true){
        if (!getline(std::cin, message) || message == Constants::COMMAND_STOP) {
            printf("Closing connection.\n");
            SendMessage(Constants::MESSAGE_END);
            m_Receiver.join();
            return;
        }

        if (m_Stopped) {
            printf("Connection closed by server.\n");
            Stop();
            return;
        }

        // regular message
        if (message.size() < Constants::COMMAND_LEN || (!message.empty() && message[0] != '/')) {
            if (!SendMessage(Constants::MESSAGE_CHANNEL, message))
                printf("Error whilst sending message: try again.\n");
            continue;
        }

        ProcessCommand(message);
    }
}

void CSocketClient::ProcessCommand(const std::string & message) {
    std::string command = message.substr(0, Constants::COMMAND_LEN),
            withoutCommand = message.substr(Constants::COMMAND_LEN);

    if (command == Constants::COMMAND_HELP) {
        printf("Usage:\n"
               "/prv <user> <message> ... send private message\n"
               "/pbl <message>        ... send public message\n"
               "<message>             ... send message to channel you are in\n"
               "/usr <username>       ... change username\n"
               "/chn <channel>        ... change channel\n"
               "/hlp                  ... display help\n"
               "/end                  ... end connection\n");
        return;
    }

    auto it = m_Commands.find(command);
    if (it == m_Commands.end()) {
        printf("Unrecognized command.\n");
        return;
    }

    if (!SendMessage(it->second, withoutCommand))
        printf("Error whilst sending command: try again.\n");
}

void CSocketClient::Stop() {
    m_Receiver.join();
    close(m_Id);
}
