#pragma once

#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h>
#include "common.h"

class CSocket {
    explicit CSocket(int id) : m_Id(id) {}
protected:
    int m_Id;
    CSocket();
public:
    int Id() const;
    bool operator < (const CSocket &) const;
    static CSocket Create(int id);

    template<typename T> bool ReadMessage(T & message) const {
        return recv(m_Id, &message, sizeof(message), 0) == sizeof(message);
    }
    bool ReadMessage(std::string &) const;
    bool SendMessage(uint32_t, const std::string & message = "") const;
};
