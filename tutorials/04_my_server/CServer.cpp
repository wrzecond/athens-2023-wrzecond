#include "CServer.h"

std::map<CSocket, CUser> CServer::m_Clients;
std::mutex CServer::m_ClientMtx;

std::map<uint32_t, std::shared_ptr<CMessage>> CServer::m_Processors = {
    {Constants::MESSAGE_END, std::make_shared<CMessageEnd>()},
    {Constants::MESSAGE_USERNAME, std::make_shared<CMessageUsername>()},
    {Constants::MESSAGE_CHG_CHL, std::make_shared<CMessageChannelChange>()},
    {Constants::MESSAGE_DM, std::make_shared<CMessageDirect>()},
    {Constants::MESSAGE_CHANNEL, std::make_shared<CMessageChannel>()},
    {Constants::MESSAGE_PUBLIC, std::make_shared<CMessagePublic>()}
};

void CServer::Print(const std::string & str) const {
    printf("%d: %s\n", m_Socket.Id(), str.c_str());
}

bool CServer::ProcessMessage(uint32_t messageId) {
    auto it = m_Processors.find(messageId);
    if (it == m_Processors.end()) // unknown message
        return m_Socket.SendMessage(Constants::RESPONSE_UNKNOWN_MESSAGE);

    const auto & processor = *(it->second);

    std::string message;
    if (processor.RequiresMessage() && !m_Socket.ReadMessage(message))
        return false;
    if (!message.empty() && message[0] == ' ')
        message = message.substr(1);

    return processor.ProcessMessage(m_Clients, m_ClientMtx, m_Socket, message);
}

void CServer::RunThread() {
    {
        std::unique_lock<std::mutex> ul (m_ClientMtx);
        m_Clients.emplace(m_Socket, CUser(m_Socket.Id(), "public", ""));
    }

    uint32_t messageId;
    while (m_Socket.ReadMessage(messageId)) {
        if (Constants::DEBUG)
            Print("Received " + std::to_string(messageId));
        if (!ProcessMessage(messageId))
            break;
    }

    {
        std::unique_lock<std::mutex> ul (m_ClientMtx);
        m_Clients.erase(m_Socket);
    }

    Print("Connection closed.");
    close(m_Socket.Id());
}
