#pragma once

#include "CMessage.h"

class CMessageChannelChange: public CMessage {
public:
    CMessageChannelChange() : CMessage(Constants::MESSAGE_CHG_CHL, true) {}
    bool ProcessMessage(std::map<CSocket, CUser> & clients, std::mutex & clientMtx,
                        CSocket & socket,std::string newChannel) const override {
        {
            std::unique_lock<std::mutex> ul (clientMtx);
            auto it = clients.find(socket);
            it->second.SetChannel(newChannel);
        }
        return socket.SendMessage(Constants::RESPONSE_CHANNEL_CHANGED);
    }
};
