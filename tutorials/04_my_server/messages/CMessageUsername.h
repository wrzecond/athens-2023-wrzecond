#pragma once

#include "CMessage.h"

class CMessageUsername: public CMessage {
public:
    CMessageUsername() : CMessage(Constants::MESSAGE_USERNAME, true) {}
    bool ProcessMessage(std::map<CSocket, CUser> & clients, std::mutex & clientMtx,
                        CSocket & socket, std::string newUsername) const override {
        {
            std::unique_lock<std::mutex> ul (clientMtx);
            auto it = clients.find(socket);
            auto it2 = std::find_if(clients.begin(), clients.end(), [&](const auto & pair) {
                return pair.second.Username() == newUsername;
            });

            if (it2 != clients.end())
                return socket.SendMessage(Constants::RESPONSE_TAKEN_USERNAME);

            it->second.SetUsername(newUsername);
        }
        return socket.SendMessage(Constants::RESPONSE_USERNAME_CHANGED);
    }
};
