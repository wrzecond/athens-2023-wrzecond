#pragma once

#include <vector>
#include "CMessage.h"

class CMessageChannel: public CMessage {
public:
    CMessageChannel() : CMessage(Constants::MESSAGE_CHANNEL, true) {}
    bool ProcessMessage(std::map<CSocket, CUser> & clients, std::mutex & clientMtx,
                        CSocket & socket, std::string message) const override {
        std::vector<std::pair<CSocket, CUser>> socketUsers;
        std::string username, channel;

        {
            std::unique_lock<std::mutex> ul (clientMtx);
            username = clients.at(socket).Username();
            channel = clients.at(socket).Channel();

            // copy users from same channel
            std::copy_if(clients.begin(), clients.end(), std::back_inserter(socketUsers), [&](const auto & pair) {
                return pair.second.Channel() == channel;
            });
        }

        if (username.empty())
            return socket.SendMessage(Constants::RESPONSE_NO_USERNAME);

        std::vector<CSocket> socketIds; // transform to sockets
        std::transform(socketUsers.begin(), socketUsers.end(), std::back_inserter(socketIds), [](const auto & pair) {
            return pair.first;
        });

        std::string msg = "#" + channel + ": " + username + " > " + message;
        for (CSocket & userSocket : socketIds)
            userSocket.SendMessage(Constants::RESPONSE_MESSAGE, msg);
        return true;
    }
};
