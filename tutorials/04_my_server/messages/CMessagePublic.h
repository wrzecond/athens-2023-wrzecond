#pragma once

#include <vector>

#include "CMessage.h"

class CMessagePublic: public CMessage {
public:
    CMessagePublic() : CMessage(Constants::MESSAGE_PUBLIC, true) {}
    bool ProcessMessage(std::map<CSocket, CUser> & clients, std::mutex & clientMtx,
                        CSocket & socket, std::string message) const override {
        std::vector<CSocket> socketIds;
        std::string username;

        {
            std::unique_lock<std::mutex> ul (clientMtx);
            std::transform(clients.begin(), clients.end(), std::back_inserter(socketIds), [](const auto & pair) {
                return pair.first;
            });
            username = clients.at(socket).Username();
        }

        if (username.empty())
            return socket.SendMessage(Constants::RESPONSE_NO_USERNAME);

        std::string msg = username + " > " + message;
        for (CSocket & userSocket : socketIds)
            userSocket.SendMessage(Constants::RESPONSE_MESSAGE, msg);
        return true;
    }
};
