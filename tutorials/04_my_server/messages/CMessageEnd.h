#pragma once

#include "CMessage.h"

class CMessageEnd: public CMessage {
public:
    CMessageEnd() : CMessage(Constants::MESSAGE_END, false) {}
    bool ProcessMessage(std::map<CSocket, CUser> &, std::mutex &, CSocket &, std::string) const override {
        return false; // stop
    }
};
