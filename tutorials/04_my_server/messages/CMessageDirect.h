#pragma once

#include <sstream>
#include "CMessage.h"

class CMessageDirect: public CMessage {
public:
    CMessageDirect() : CMessage(Constants::MESSAGE_DM, true) {}
    bool ProcessMessage(std::map<CSocket, CUser> & clients, std::mutex & clientMtx,
                        CSocket & socket,std::string message) const override {
        std::stringstream ss (message);
        std::string username, rawMessage, myUsername;

        if (!(ss >> username))
            return socket.SendMessage(Constants::RESPONSE_PARSE_ERROR);

        const CSocket * sendSocket;
        {
            std::unique_lock<std::mutex> ul (clientMtx);
            myUsername = clients.at(socket).Username();
            auto it = std::find_if(clients.begin(), clients.end(), [&](const auto & item) {
                return item.second.Username() == username;
            });
            sendSocket = it != clients.end() ? &(it->first) : nullptr;
        }
        if (!sendSocket)
            return socket.SendMessage(Constants::RESPONSE_UNKNOWN_USER);

        std::getline(ss, rawMessage);
        return sendSocket->SendMessage(Constants::RESPONSE_MESSAGE, myUsername + " >" + rawMessage);
    }
};
