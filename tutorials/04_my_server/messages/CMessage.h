#pragma once

#include <map>
#include <string>
#include <thread>

#include "../CSocket.h"
#include "../CUser.h"

class CMessage {
    uint32_t m_Id;
    bool m_RequiresMessage;
public:
    CMessage (uint32_t id, bool requiresMessage)
    : m_Id(id), m_RequiresMessage(requiresMessage) {}
    virtual ~CMessage() = default;
    uint32_t Id() const {
        return m_Id;
    }
    bool RequiresMessage() const {
        return m_RequiresMessage;
    }
    virtual bool ProcessMessage(std::map<CSocket, CUser> &, std::mutex &, CSocket &, std::string) const = 0;
};
