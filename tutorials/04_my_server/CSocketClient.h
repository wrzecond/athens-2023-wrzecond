#pragma once

#include <iostream>
#include <map>
#include <netdb.h>
#include <thread>

#include "CSocket.h"

class CSocketClient : public CSocket {
    std::thread m_Receiver;
    std::atomic_bool m_Stopped;
    static std::map<std::string, uint32_t> m_Commands;
    static std::map<uint32_t, std::string> m_Messages;

    void Receiver();
    void ProcessCommand(const std::string &);
public:
    explicit CSocketClient();

    bool Connect(const char * hostname = Constants::DEFAULT_HOSTNAME, int port = Constants::DEFAULT_SERVER_PORT);
    void Reset();

    bool SetUsername (const std::string &);
    void Start();
    void Stop();
};
