#pragma once

#include <stdexcept>
#include <sstream>
#include <string>

namespace Constants {
    static const uint32_t MESSAGE_USERNAME = 0;
    static const uint32_t MESSAGE_DM       = 1;
    static const uint32_t MESSAGE_PUBLIC   = 2;
    static const uint32_t MESSAGE_CHANNEL  = 3;
    static const uint32_t MESSAGE_END      = 4;
    static const uint32_t MESSAGE_CHG_CHL  = 5;

    static const uint32_t RESPONSE_USERNAME_CHANGED = 1000;
    static const uint32_t RESPONSE_MESSAGE          = 1001;
    static const uint32_t RESPONSE_CHANNEL_CHANGED  = 1002;
    static const uint32_t RESPONSE_UNKNOWN_MESSAGE  = 1003;
    static const uint32_t RESPONSE_PARSE_ERROR      = 1004;
    static const uint32_t RESPONSE_UNKNOWN_USER     = 1005;
    static const uint32_t RESPONSE_NO_USERNAME      = 1006;
    static const uint32_t RESPONSE_TAKEN_USERNAME   = 1007;

    static const char * DEFAULT_HOSTNAME = "localhost";
    static const int DEFAULT_SERVER_PORT = 20000;
    static const int BUFFER_SIZE         = 1024;
    static const bool DEBUG              = false;

    static const char * COMMAND_STOP      = "/end";
    static const char * COMMAND_DM        = "/prv";
    static const char * COMMAND_USERNAME  = "/usr";
    static const char * COMMAND_PUBLIC    = "/pbl";
    static const char * COMMAND_CHG_CHL   = "/chn";
    static const char * COMMAND_HELP      = "/hlp";
    static const size_t COMMAND_LEN = 4;
}

class SocketException: public std::runtime_error {
protected:
    explicit SocketException(const char * what)
    : std::runtime_error(what) {}
};

class SocketCreateException : public SocketException {
public:
    SocketCreateException()
    : SocketException("Could not Create socket.") {}
};

class SocketBindException : public SocketException {
public:
    explicit SocketBindException(int port)
    : SocketException(("Could not bind to port " + std::to_string(port) + ".").c_str()) {}
};

class SocketPassiveException : public SocketException {
public:
    SocketPassiveException()
    : SocketException("Could not mark socket as passive.") {}
};
